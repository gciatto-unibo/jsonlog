package it.unibo.jsonlog

object R {
  object theories {
    lazy val typeSystem: String =
      """
        | obj(L) :- length(L, _).
        |obj(L, N) :- iter(N, I), length(L, I).
        |obj(L, obj(R)) :- obj(L), obj(R).
        |obj(L, N, obj(R)) :- iter(N, I), J is N - I, length(L, J), length(R, I).
        |
        |kv(K, V) :- var(K); atom(K).
        |
        |match(kv(K1, V1), kv(K1, V2)) :-
        |  match(K1, K2),
        |  match(V1, V2).
        |
        |match(obj(S1), obj(S2)) :-
        |  var(S1), nonvar(S2), !,
        |  match(obj(S2), obj(S1)).
        |match(obj(S1), obj(S2)) :-
        |  nonvar(S1), !, length(S1, N),
        |  obj(S1), obj(S2, N),
        |  sameobj(S1, S2).
        |
        |match(obj(S1), obj(S2, obj(R2))) :-
        |  var(S1), nonvar(S2), nonvar(R2), !,
        |  match(obj(S2, obj(R2)), obj(S1)).
        |match(obj(S1), obj(S2, obj(R2))) :-
        |  nonvar(S1), !, length(S1, N),
        |  obj(S1), obj(S2, N, obj(R2)),
        |  superobj(S1, S2, R3),
        |  sameobj(R2, R3).
        |
        |match(obj(S1, obj(R1)), obj(S2)) :-
        |  (var(S1); var(R1)), nonvar(S2), !,
        |  match(obj(S2), obj(S1, obj(R1))).
        |match(obj(S1, obj(R1)), obj(S2)). :-
        |  nonvar(S1), nonvar(R1), !, length(S1, NS), length(R1, NR), N is NS + NR,
        |  obj(S1, obj(R1)), obj(S2, N),
        |  subobj(S1, S2, R3),
        |  sameobj(R1, R3).
        |
        |match(obj(S1, obj(R1)), obj(S2, obj(R2))) :-
        |  (var(S1); var(R1)), nonvar(S2), nonvar(R2), !,
        |  match(obj(S2, obj(R2)), obj(S1, obj(R1))).
        |match(obj(S1, obj(R1)), obj(S2, R2)) :-
        |  nonvar(S1), nonvar(R1), !, length(S1, NS), length(R1, NR), N is NS + NR,
        |  obj(S1, obj(R1)), obj(S2, N, R2),
        |  sameobj(S1, S2),
        |  match(obj(R1), R2).
        |
        |match(X, Y) :- !,
        |	X = Y.
        |
        |equals(X, Y) :- match(X, Y).
        |
        |iter(N, R) :- iter(0, N, R).
        |iter(X, X, X) :- !.
        |iter(X, _, X).
        |iter(X, Y, Z) :-
        |  X < Y,
        |  X1 is X + 1,
        |  iter(X1, Y, Z).
        |
        |member(E, L, R) :-
        |  member(E, L, [], R).
        |member(E, [X | Xs], L, R) :-
        |  equals(E, X),
        |  append(L, Xs, R).
        |member(E, [X | Xs], L, R) :-
        |	append(L, [X], L1),
        |	member(E, Xs, L1, R).
        |
        |superobj([], [], []).
        |superobj([X | Xs], [], [X | Xs]).
        |superobj([X | Xs], [Y | Ys], R) :-
        |  L = [X | Xs],
        |  member(Y, L, L1),
        |  superobj(L1, Ys, R).
        |
        |subobj(A, B, R) :- superobj(B, A, R).
        |
        |sameobj([], []).
        |sameobj([_ | _], []) :- !, fail.
        |sameobj([X | Xs], [Y | Ys]) :-
        |  member(Y, [X | Xs], L1),
        |  sameobj(L1, Ys).
      """.stripMargin
  }
}
