package it.unibo.jsonlog

sealed abstract class Matching

case class Yes(unified: JsonLog, substitution: Map[Placeholder, JsonLog]) extends Matching
case object No extends Matching
