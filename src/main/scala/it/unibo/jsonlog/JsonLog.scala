package it.unibo.jsonlog

import alice.tuprolog.{NoMoreSolutionException, Number, Prolog, SolveInfo, Struct, Term, Theory, Var}
import tuprolog.Utils

import scala.collection.JavaConverters.asScalaBuffer

sealed abstract class JsonLog {
  import it.unibo.jsonlog.JsonLog.prolog

  override def toString: String = this match {
    case KeyValue(k, v) => {
      k match {
        case Left(s) => f""""$s": $v"""
        case Right(p) => f"$p: $v"
      }
    }
    case Variable(n) => f"?${n.name.toUpperCase()}"
    case Whatever => "_"
    case a: Array => a.items.mkString("[ ", ", ", " ]")
    case pa: PartialArray => pa.rest match {
      case Left(rest) => f"[ ${pa.items.mkString(", ")} | [ ${rest.items.mkString(", ")} ] ]"
      case Right(variable) => f"[ ${pa.items.mkString(", ")} | ${variable.toString} ]"
    }
    case o: Object => o.items.mkString("{ ", ", ", " }")
    case po: PartialObject => po.rest match {
      case Left(rest) => f"{ ${po.items.mkString(", ")} | { ${rest.items.mkString(", ")} } }"
      case Right(variable) => f"{ ${po.items.mkString(", ")} | ${variable.toString} }"
    }
    case Atom(s) => s
    case Numeric(x) => x.toString()
    case Truth(b) => b.toString
  }

  def toTerm: Term = this match {
    case KeyValue(k, v) => k match {
      case Left(s) => new Struct("kv", new Struct(s), v.toTerm)
      case Right(p) => new Struct("kv", p.toTerm, v.toTerm)
    }
    case Variable(n) => new Var(n.name.toUpperCase)
    case Whatever => new Var()
    case a: Array => new Struct(a.items.map(_.toTerm).toArray)
    case pa: PartialArray => {
      val restTerm: Term = pa.rest match {
        case Left(a) => a.toTerm
        case Right(p) => p.toTerm
      }
      new Struct((pa.items.map(_.toTerm) ++ Seq(restTerm)).toArray)
    }
    case o: Object => new Struct("obj", new Struct(o.items.map(_.toTerm).toArray))
    case po: PartialObject => {
      val restTerm: Term = po.rest match {
        case Left(a) => a.toTerm
        case Right(p) => p.toTerm
      }
      new Struct("obj", new Struct(po.items.map(_.toTerm).toArray), restTerm)
    }
    case Truth(b) => new Struct(if (b) "true" else "fail")
    case Atom(s) => new Struct(s)
    case Numeric(n) =>
      if (n.isValidInt) new alice.tuprolog.Int(n.toInt)
      else if (n.isExactDouble) new alice.tuprolog.Double(n.toDouble)
      else if (n.isValidLong) new alice.tuprolog.Long(n.toLong)
      else throw new IllegalArgumentException("" + n)
  }

  def <=>(other: JsonLog): Matching = {
    val si: SolveInfo = prolog.solve(new Struct("match", this.toTerm, other.toTerm))
    prolog.solveEnd()
    getSolution(si)
  }

  private def getSolution(si: SolveInfo): Matching = {
    if (si.isSuccess) {
      val matching = JsonLog(si.getSolution.asInstanceOf[Struct].getArg(0))
      val bindings = asScalaBuffer(si.getBindingVars).toStream
        .filter(v => !v.isAnonymous)
        .map(v => (Variable(Symbol(v.getName)): Placeholder, JsonLog(v.getLink)))
        .map(kv => Map(kv))
        .foldLeft(Map.empty[Placeholder, JsonLog])((a, b) => a ++ b)

      Yes(matching, bindings)
    } else {
      No
    }
  }

  def <==>(other: JsonLog): Stream[Matching] = {
    val si: SolveInfo = prolog.solve(new Struct("match", this.toTerm, other.toTerm))

    Stream(getSolution(si)) ++ Stream.continually{
      try {
        getSolution(prolog.solveNext())
      } catch {
        case _: NoMoreSolutionException => No
      }
    }.map(matching => {
      if (matching == No) prolog.solveEnd()
      matching
    }).takeWhile(_.isInstanceOf[Yes])
  }
}

case class KeyValue(key: Either[String, Placeholder], value: JsonLog) extends JsonLog

sealed abstract class Placeholder extends JsonLog

case class Variable(name: Symbol) extends Placeholder

case object Whatever extends Placeholder

sealed abstract class Sequence extends JsonLog

case class Array(items: JsonLog*) extends Sequence

case class PartialArray(items: Seq[JsonLog], rest: Either[Array, Placeholder]) extends Sequence

sealed abstract class Multiset extends JsonLog

case class Object(items: JsonLog*) extends Multiset {
  def | (placeholder: Placeholder) = ???
}

case class PartialObject(items: Seq[JsonLog], rest: Either[Object, Placeholder]) extends Multiset

object PartialObject {
  def apply(items: JsonLog*): PartialObject = new PartialObject(items, Right(Whatever))

//  def apply(items: JsonLog*)(rest: Placeholder): PartialObject = new PartialObject(items, Right(rest))
//
//  def apply(items: JsonLog*)(rest: Object): PartialObject = new PartialObject(items, Left(rest))

//  def apply(items: JsonLog*)(rest0: JsonLog, rest: JsonLog*): PartialObject = new PartialObject(items, Left(Object(rest: _*)))
}

sealed abstract class Value extends JsonLog

case class Atom(value: String) extends Value

case class Numeric(value: BigDecimal) extends Value

case class Truth(value: Boolean) extends Value

object JsonLog {

  private lazy val prolog: Prolog = {
    val prolog = new Prolog()
    prolog.setTheory(new Theory(R.theories.typeSystem))
    prolog
  }

  def apply(term: Term): JsonLog = term match {
    case struct: Struct => struct.getName match {
      case "kv" => struct.getArg(0) match {
        case s: Struct => KeyValue(Left(s.getName), JsonLog(struct.getArg(1)))
        case v: Var =>
          if (v.isBound) KeyValue(Left(v.getLink.asInstanceOf[Struct].getName), JsonLog(struct.getArg(1)))
          else KeyValue(Right(JsonLog(v).asInstanceOf[Placeholder]), JsonLog(struct.getArg(1)))
      }
      case "." => Array(Utils.flatten(struct).map(JsonLog(_)): _*)
      case "obj" =>
        val elements: Stream[JsonLog] = Utils.flatten(struct.getArg(0)).map(JsonLog(_))
        if (struct.getArity() == 1) {
          Object(elements: _*)
        } else {
          JsonLog(struct.getArg(1)) match {
            case p: Placeholder => PartialObject(elements, Right(p))
            case o: Object => PartialObject(elements, Left(o))
            case _ => throw new IllegalArgumentException()
          }
        }
      case "true" => Truth(true)
      case "fail" => Truth(false)
      case _ => Atom(struct.getName)
    }
    case number: Number =>
      if (number.isInteger) Numeric(number.longValue())
      else if (number.isReal) Numeric(number.doubleValue())
      else throw new IllegalArgumentException(number.toString)
    case variable: Var =>
      if (variable.isBound) JsonLog(variable.getLink)
      else if (variable.isAnonymous) Whatever
      else Variable(Symbol(variable.getName))
  }

  def ?(): Placeholder = Whatever

  def ?(name: Symbol): Variable = Variable(name)

  implicit class PlaceholderToKv(variable: Placeholder) {
    def ->(other: JsonLog) = KeyValue(Right(variable), other)
  }

  implicit class StringToKv(name: String) {
    def ->(other: JsonLog) = KeyValue(Left(name), other)
  }

  implicit def setToObject(items: Seq[JsonLog]) = Object(items: _*)

  implicit def seqToArray(items: Seq[JsonLog]) = Array(items: _*)

  implicit def stringToAtom(string: String) = Atom(string)

  implicit def booleanToThruth(bool: Boolean) = Truth(bool)

  implicit def numberToNumeric(x: Int) = Numeric(x)

  implicit def numberToNumeric(x: Long) = Numeric(x)

  implicit def numberToNumeric(x: Float) = Numeric(x + 0.0)

  implicit def numberToNumeric(x: Double) = Numeric(x)

  implicit def numberToNumeric(x: Byte) = Numeric(x + 0)
}

