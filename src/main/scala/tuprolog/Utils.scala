package tuprolog

import alice.tuprolog.{Struct, Term}

object Utils {

  def flatten(list: Struct): Stream[Term] = {
    val i = list.listIterator()
    Stream.continually({ if (i.hasNext) Some(i.next()) else None })
        .takeWhile(_.isDefined)
        .map(_.get)
  }

  def flatten(term: Term): Stream[Term] = term match {
    case struct: Struct => flatten(struct)
    case _ => Stream(term)
  }
}
